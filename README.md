gamesPlayed.py is a utility that connects to the chess.com API. Documentation of that API can be found here: https://www.chess.com/news/view/published-data-api. When the script is run, the user is presented with a menu prompting them to enter the name of the chess team being analyzed. This name must be all lower case and hyphenated so no spaces exist. For example "the-golden-phoenix". On rare occasions, the name in the team's URL will be the string "group-" followed by a unique identifier. for example "group-0000000058ddf6500000000028339089" which is the identifier of a team called Ninja Empire.

Once the proper name is entered, the script connects to the API endpoint https://api.chess.com/pub/club/{team name}/members. The returned JASON file is parsed, and the following data is stored in a dictionary.
Date Joined: date the member joinded the team
Activity: how frequently the member has been active in the club. 
User Name: the members user name

Then, for each user name in that dictionary a second API endpoint is connected to called: https://api.chess.com/pub/player/{user name}/matches. this endpoint contains every match that member has ever played regardless of which team the member was playing for. The returned JASON file is parsed and for each match played, the name of the team that member was playing for is compaired to the name of the team being analyzed. If they match, then a counter is incremented. Once all matches are counted, the following data is written to another dictionary.
user name: The user name of the member
Date Joined: The date the member joined that club
Activity: how frequently the member has been active in the club.
Status: The status of the match. Values are: finished, in_progress, and registered

Next, the Date Joined field is converted from UNIX time to a readable timestamp and all the data is written out to a CSV file in the scripts home directory.